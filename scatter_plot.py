import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import rcParams
# seaborn configuration
color_palette = sns.color_palette("muted")
sns.set_palette(color_palette)
sns.set_style("whitegrid")
sns.set_context("paper", font_scale=1.10, rc={"lines.linewidth": 1.5, "figure.figsize":(8, 4.5), "figure.dpi": 150})
# matplotlib configuration
rcParams["figure.figsize"] = (8, 5)
rcParams["figure.dpi"] = 150

from nilmtk import DataSet
from nilmtk.utils import print_dict

ampd = DataSet("./AMPD/AMPds.h5")

# setting start and end dates
start_date = "2012-04-01"
end_date = "2012-04-07"

ampd.set_window(start=start_date, end=end_date)
bdn1 = ampd.buildings[1]

elec = bdn1.elec

df_list = []

for i in range(2, 22):
    appliance = elec[i]
    appliance_name = appliance.appliances[0].identifier.type
    appliance_id = appliance.appliances[0].identifier.instance
    if appliance_name != 'unknown':
        continue
    appliance_name = f"{appliance_name} {appliance_id}"
    
    print(f"{i} \t {appliance_name}")
    
    active = next(appliance.power_series(ac_type='active'))
    reactive = next(appliance.power_series(ac_type='reactive'))
    
    df = pd.DataFrame({
        'active': active,
        'reactive': reactive,
        'appliance': appliance_name
    })
    
    df_list.append(df)
df = pd.concat(df_list)
df = df.drop_duplicates()

fig = plt.figure(dpi=150)
sns.scatterplot('active', 'reactive', data=df, hue='appliance')
plt.legend(loc=1, ncol=2)
plt.show()