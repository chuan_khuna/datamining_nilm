import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import rcParams
# seaborn configuration
color_palette = sns.color_palette("muted")
sns.set_palette(color_palette)
sns.set_style("whitegrid")
sns.set_context("paper", font_scale=1.10, rc={"lines.linewidth": 1.5, "figure.figsize":(8, 4.5), "figure.dpi": 150})
# matplotlib configuration
rcParams["figure.figsize"] = (8, 5)
rcParams["figure.dpi"] = 150

from nilmtk import DataSet
from nilmtk.utils import print_dict

ampd = DataSet("./AMPD/AMPds.h5")

# setting start and end dates
start_date = "2012-04-01"
end_date = "2012-04-07"

ampd.set_window(start=start_date, end=end_date)
bdn1 = ampd.buildings[1]

elec = bdn1.elec
print(elec)

top_elec = elec.select_top_k(k=11)
top_elec.plot()
plt.show()

